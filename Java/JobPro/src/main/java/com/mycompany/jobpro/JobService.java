/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jobpro;

import java.time.LocalDate;

/**
 *
 * @author user
 */
public class JobService {
    public static boolean checkEnableTime(LocalDate strat, LocalDate end, LocalDate today){
        if(today.isBefore(strat)){
            return false;
        }
        if(today.isAfter(end)){
            return false;
        }
        return true;
    }
}
